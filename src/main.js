/* 
 * TODO:
 *      - detect unsaved changes
 *      - fix local time when editing entries
 */

//
// prototypes
//

// pad numbers with decimal zeros
Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

//
// GLOBALS
//

// elements
const d = document;
const main = d.getElementById("main");
const accTabs = d.getElementById("account_tabs");
const addBtn = d.getElementById("account_add");
const editBtn = d.getElementById("account_edit");
const saveBtn = d.getElementById("file_save");
const exportBtn = d.getElementById("file_export");
const dialogAccEdit = d.getElementById("dialog_account_edit");
const dialogEntryEdit = d.getElementById("dialog_entry_edit");
const dialogFileExport = d.getElementById("dialog_file_export");

// account
let accounts = [];
let balance;
let active = 0;
let changes = true;