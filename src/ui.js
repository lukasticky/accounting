//
// UI
//

// render current account's entries
function render(posAcc) {
    
    // clear `main`
    main.innerHTML = "";
    
    // create `tr`-elements and append them to DOM
    let tr;
    let date;
    if (accounts[posAcc].entries != null) {
        for (let i = 0; i < accounts[posAcc].entries.length; i++) {
            tr = d.createElement("tr");
            date = new Date(accounts[posAcc].entries[i].date);
            tr.innerHTML = `<td>${date.toLocaleDateString()} ${date.toLocaleTimeString()}</td>
            <td>${accounts[posAcc].entries[i].text}</td>
            <td>${accounts[posAcc].entries[i].credit.toFixed(accounts[posAcc].currencyDecimals)} ${accounts[posAcc].currency}</td>`;
            
            
            if (accountBalance(posAcc, i+1).toFixed(accounts[posAcc].currencyDecimals) < 0) {
                tr.innerHTML += `<td negative>${accountBalance(posAcc, i+1).toFixed(accounts[posAcc].currencyDecimals)} ${accounts[posAcc].currency}</td>`;
            } else {
                tr.innerHTML += `<td>${accountBalance(posAcc, i+1).toFixed(accounts[posAcc].currencyDecimals)} ${accounts[posAcc].currency}</td>`;
            }
            
            // prepend `tr` elements to DOM
            main.insertBefore(tr, main.childNodes[0] || null);
        }
    }
    
    // create "add entry"-`tr` and append to DOM
    tr = d.createElement("tr");
    tr.innerHTML = `<td><button onclick="showDialogEntryEdit(accounts[active].entries.length);">&nbsp;+&nbsp;</button></td><td></td><td></td><td></td>`;
    main.insertBefore(tr, main.childNodes[0] || null);
    
    // create header row and append to DOM
    tr = d.createElement("tr");
    tr.innerHTML = `<th>Date</th><th>Text</th><th>Credit</th><th>Balance</th>`;
    main.insertBefore(tr, main.childNodes[0] || null);
    
    // add `onclick`-event to `tr`-elements in `main`
    addTrOnclick();
}

// switch to account
function switchTo(posAcc) {

    // set active account
    active = posAcc;

    // toggle `active`-class of tabs
    for (let i = 0; i < accounts.length; i++) {
        accTabs.childNodes[i].classList.remove("active");    
    }
    accTabs.childNodes[active].classList.add("active");

    // render active account
    render(active);
}

function enableButtons() {

    // enable buttons
    addBtn.disabled = false;
    editBtn.disabled = false;
    saveBtn.disabled = false;
    exportBtn.disabled = false;
}

//
// DIALOGS
//

// show dialog for editing (& creating) accounts
function showDialogAccountEdit(posAcc) {

    // show dialog
    dialogAccEdit.showModal();

    // set "data-*"
    dialogAccEdit.dataset.posAcc = posAcc;
    
    // fill out form
    if (accounts[posAcc] == null) {
        d.getElementById("dialog_account_edit_name").value = "";
        d.getElementById("dialog_account_edit_currency").value = "";
        d.getElementById("dialog_account_edit_currency_decimals").value = 2;
    } else {
        d.getElementById("dialog_account_edit_name").value = accounts[posAcc].name;
        d.getElementById("dialog_account_edit_currency").value = accounts[posAcc].currency;
        d.getElementById("dialog_account_edit_currency_decimals").value = accounts[posAcc].currencyDecimals;
    }
}

// save account cahnges
function saveDialogAccountEdit() {

    // set posAcc
    const posAcc = dialogAccEdit.dataset.posAcc;
    
    // create account
    let account = {
        "name": d.getElementById("dialog_account_edit_name").value,
        "currency": d.getElementById("dialog_account_edit_currency").value,
        "currencyDecimals": Number(d.getElementById("dialog_account_edit_currency_decimals").value)
    };

    // apply changes
    if (accounts[posAcc] == null) {

        // create entry array
        account.entries = [];

        accountAdd(account);
    } else {
        accountEdit(posAcc, account);
    }

    // hide modal
    d.getElementById("dialog_account_edit").close();

    
    // rerender everything
    switchTo(posAcc);
    render(posAcc);
}

// add `onclick`-event to `tr`-elements in `main`
function addTrOnclick() {
    
    // get `tr`-elements from `main`
    let tr = main.getElementsByTagName("tr");
    
    // apply onclick to each `tr`
    for (let i = 2; i < tr.length; i++) {
        tr[i].onclick = function() {
            return showDialogEntryEdit(tr.length - i - 1);
        }
    }
    
}

// show dialog for editing (& creating) entries
function showDialogEntryEdit(posEntry) {
    
    // set posAcc
    const posAcc = active;

    // show dialog
    dialogEntryEdit.showModal();

    // set "data-*"
    dialogEntryEdit.dataset.posEntry = posEntry;
    dialogEntryEdit.dataset.posAcc = posAcc;
    
    // fill out form
    if (accounts[posAcc].entries[posEntry] == null) {
        d.getElementById("dialog_entry_edit_date").value = new Date().toISOString().slice(0, -1);
        d.getElementById("dialog_entry_edit_text").value = "";
        d.getElementById("dialog_entry_edit_credit").value = 0;
    } else {
        d.getElementById("dialog_entry_edit_date").value = accounts[posAcc].entries[posEntry].date.slice(0, -1);
        d.getElementById("dialog_entry_edit_text").value = accounts[posAcc].entries[posEntry].text;
        d.getElementById("dialog_entry_edit_credit").value = accounts[posAcc].entries[posEntry].credit;
    }
}

// save entry changes
function saveDialogEntryEdit() {

    // set posAcc
    const posAcc = dialogEntryEdit.dataset.posAcc;
    const posEntry = dialogEntryEdit.dataset.posEntry;
    
    let entry = {
        "date": d.getElementById("dialog_entry_edit_date").value,
        "text": d.getElementById("dialog_entry_edit_text").value,
        "credit": Number(d.getElementById("dialog_entry_edit_credit").value)
    }
    
    // create entry if necessary
    if (accounts[posAcc].entries[posEntry] == null) {
        entryAdd(posAcc, entry);
    } else {
        entryEdit(posAcc, posEntry, entry);
    }

    // hide modal
    d.getElementById("dialog_entry_edit").close();

    // rerender everything
    render(posAcc);
}

// file export
function fileExport() {

    // get select element
    let select =  d.getElementById("dialog_file_export_type");

    // convert
    let ex;
    switch (select.value) {
        case "csv":
            ex = convertToDSV(",");
            break;
        case "tsv":
            ex = convertToDSV('&Tab;');
            break;
        case "dsv":
            ex = convertToDSV();
            break;
        case "md":
            ex = convertToMD();
            break;
        default:
            ex = "";
    }

    // copy `ex` to clipboard
    let res = d.getElementById("dialog_file_export_result");
    res.innerHTML = ex;
    res.select();
    d.execCommand("copy");
    dialogFileExport.close(); // close dialog
}

// `fileLoad()` on file upload
d.getElementById("file_load_input").addEventListener("change", fileLoad, false);



// complain about unsaved changes
function unsavedChanges() {

}

// handle "user" errors
function userError(message, error = "") {
    alert(`${message || "Unexpected Error"}\n${error}`);
}
