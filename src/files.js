//
// FILES
//

// create new file
function fileNew() {

    // clear `.tab_btn` elements
    for (let i = 0; i < accounts.length;) {
        accountDel(0); 
    }

    enableButtons();

    // clear `main`
    main.innerHTML = ``;

    showDialogAccountEdit(0);
}

// load & parse uploaded file
function fileLoad() {

    let file = d.getElementById("file_load_input").files[0];
    let reader = new FileReader();

    // executes when the file is loaded
    reader.onloadend = function(e) {
        if (e.target.readyState == FileReader.DONE) {
            
            // parse JSON
            let json;
            try {
                json = JSON.parse(e.target.result);
            } catch(error) {
                userError("File type not supported");
            }

            // clear `.tab_btn` elements
            for (let i = 0; i < accounts.length;) {
                accountDel(0); 
            }
            
            // create accounts
            accounts = [];
            for (account of json) {
                accountAdd(account);
            }

            enableButtons();
            
            // clear `main`
            main.innerHTML = ``;

            // switch to first account
            switchTo(0);
        }
    };

    // start the reading process
    let blob;
    try {
        blob = file.slice(0, file.size);
        reader.readAsText(blob);
    } catch(error) {
        if (blob == null) {
        } else {
            userError("Something happened! Check the error code below for more information:", error);
        }
    }
}

// save `accounts[]` to file & download
function fileSave(contents, filename, filetype) {
    let a = d.createElement("a");
    let file = new Blob([contents], {type: filetype});
    a.href = URL.createObjectURL(file);
    a.download = filename;
    a.click();
}