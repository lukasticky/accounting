//
// CONVERT
//

function convertToDSV(delimiter) {

    // set posAcc
    const posAcc = active;
    
    // convert accounts to DSV
    let text = `"Date"${delimiter}"Text"${delimiter}"Credit"\n`;
    for (let i = 0; i < accounts[posAcc].entries.length; i++) {
        text += `"${accounts[posAcc].entries[i].date}"${delimiter}"${accounts[posAcc].entries[i].text.replace(/"/g, "\"\"")}"${delimiter}"${accounts[posAcc].entries[i].credit.toFixed(accounts[posAcc].currencyDecimals)} ${accounts[posAcc].currency}"\n`;
    }

    return text;
}

function convertToMD() {
    
    // set posAcc
    const posAcc = active;
    
    // convert accounts to DSV
    let text = `| Date | Text | Credit |\n|-|-|-:|\n`;
    for (let i = 0; i < accounts[posAcc].entries.length; i++) {
        text += `| ${accounts[posAcc].entries[i].date} | ${accounts[posAcc].entries[i].text.replace(/"/g, "\"\"")} | ${accounts[posAcc].entries[i].credit.toFixed(accounts[posAcc].currencyDecimals)} ${accounts[posAcc].currency} |"\n`;
    }

    return text;
}