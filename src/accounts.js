//
// ACCOUNTS
//

// add account to `accounts[]`
function accountAdd(account) {

    // append account to accounts
    const posAcc = accounts.push(account) - 1;

    // create `.tab_btn` element
    let tab = d.createElement("button");
    tab.classList.add("tab_btn");
    tab.setAttribute("onclick", `switchTo(${posAcc})`);
    tab.innerText = accounts[posAcc].name;

    // append tab to DOM
    accTabs.insertBefore(tab, addBtn);
}

// add changes to account in `accounts[]`
function accountEdit(posAcc, account) {
    
    // update account
    accounts[posAcc] = {...accounts[posAcc], ...account};
    
    // update tab text
    let tab = d.getElementsByClassName("tab_btn")[posAcc];
    tab.innerText = accounts[posAcc].name;
}

// delete account from `accounts[]`
function accountDel(posAcc) {

    // delete account
    accounts.splice(posAcc, 1);

    // delete `.tab_btn` element
    d.getElementsByClassName("tab_btn")[posAcc].remove();
}

// calculate balance
function accountBalance(posAcc, posEntry = accounts[posAcc].entries.length) {
    balance = 0;
    for (let i = 0; i < posEntry; i++) {
        balance += accounts[posAcc].entries[i].credit;
    }
    return balance;
}