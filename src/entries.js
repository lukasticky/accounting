//
// ENTRIES
//

// add entry to account
function entryAdd(posAcc, entry) {
    return posEntry = accounts[posAcc].entries.push(entry) - 1;
}

// add changes to entry in account
function entryEdit(posAcc, posEntry, entry) {
    accounts[posAcc].entries[posEntry] = entry;
}

// delete entry from account
function entryDel(posAcc, posEntry) {
    accounts[posAcc].entries.splice(posEntry);
}