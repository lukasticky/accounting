# Accounting

Just a small side project to write down spendings or similar

**Only Chromium is supported**, because HTML dialogs don't fully work on other browsers _yet_.

![Screenshot](Screenshot.png)

## How to use it

You'll need a webserver to run this little thing. If you have PHP installed, just cd into the `src`-directory and type:

```bash
$ php -S localhost:8000
```

Otherwise you can also try it on [kasticky.com/accounting](https://kasticky.com/accounting)

## Usage

The code isn't that good, hasn't been properly tested and is **not suitable for serious usage**.
